<?php

class Clientes_model extends CI_Model
{
    private $table = 'clientes';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($id = null) {
        if ($id === null) {
            $query = $this->db->get($this->table);
            return $query->result();
        }

        $query = $this->db->get_where($this->table, array('id' => $id));

        return $query->first_row();
    }

    public function novo() {
        $data = array(
            'nome' => $this->input->post('nome'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone'),
            'foto' => $_FILES['foto']['name']
        );

        return $this->db->insert($this->table, $data);
    }

    public function atualizar($id) {
        $data = array(
            'nome' => $this->input->post('nome'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone'),
        );

        $this->db->where('id', $id);

        return $this->db->update($this->table, $data);
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id' => $id));
    }
}