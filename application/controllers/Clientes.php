<?php
class Clientes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientes_model');
    }

    public function index() {

        $rs = $this->clientes_model->get();

        $this->load->view('template/header');
        $this->load->view('cliente/index', array('clientes' => $rs));
        $this->load->view('template/footer');
    }

    public function novo() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nome', 'Nome', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/header');
            $this->load->view('cliente/novo');
            $this->load->view('template/footer');
        } else {

            $foto_nome = rand().'.jpg';
            $config = array(
                "upload_path" => "public/fotos",
                "allowed_types" => "jpg|gif|jpeg|png",
                "file_name" => $foto_nome,
                "max_size" => 300000
            );

            $_FILES['foto']['name'] = $foto_nome;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('foto') === true)
                echo $this->upload->display_erros();

            $data['back'] = '/clientes';
            $this->clientes_model->novo();
            $this->load->view('template/sucesso', $data);
        }
    }

    public function editar($id = null) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nome', 'Nome', 'required');

        if ($this->form_validation->run() === false) {

            $rs = $this->clientes_model->get($id);

            $this->load->view('template/header');
            $this->load->view('cliente/editar', array('cliente' => $rs));
            $this->load->view('template/footer');
        } else {
            $data['back'] = '/clientes/' . $id;
            $this->clientes_model->atualizar($id);
            $this->load->view('template/sucesso', $data);
        }
    }

    public function delete($id) {
        $data['back'] = '/clientes/';
        $this->clientes_model->delete($id);
        $this->load->view('template/sucesso', $data);
    }
}