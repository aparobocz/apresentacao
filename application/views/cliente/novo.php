<h2>Novo cliente</h2>

<?php echo validation_errors(); ?>

<form action="/clientes/novo" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" name="nome" class="form-control" placeholder="Digite seu nome" />
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="text" name="email" class="form-control" placeholder="Digite seu e-mail" />
    </div>
    <div class="form-group">
        <label for="nome">Telefone</label>
        <input type="text" name="telefone" id="telefone" onkeyup="mascara( this, mtel );" maxlength="15" class="form-control" placeholder="Digite seu telefone" />
    </div>

    <div class="form-group">
        <label for="foto">Foto</label>
        <input type="file" name="foto">
    </div>

    <input type="submit" value="Salvar" name="salvar" class="btn btn-info">
    <a href="/clientes" class="btn btn-link">Voltar</a>
</form>
