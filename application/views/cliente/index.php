<a href="/clientes/novo" class="btn btn-link">Cadastrar</a>

<table class="table table-hover">
    <tr>
        <th>#</th>
        <th width="100"> </th>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Telefone</th>
        <th>Ações</th>
    </tr>
    <?php foreach($clientes as $cliente) : ?>
    <tr>
        <td><?=$cliente->id?></td>
        <td><img src="/fotos/<?=$cliente->foto?>" width="100"></td>
        <td>
            <?=$cliente->nome?>
        </td>
        <td><?=$cliente->email?></td>
        <td><?=$cliente->telefone?></td>
        <td class="text-right">
            <a href="/clientes/editar/<?=$cliente->id?>" class="btn btn-xs btn-info">Editar</a>
            <form action="/clientes/delete/<?=$cliente->id?>" method="post" style="display: inline-block">
                <input type="submit" value="Remover" class="btn btn-danger btn-xs">
            </form>
        </td>
    </tr>
    <?php endforeach; ?>
</table>